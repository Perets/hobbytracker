//
//  Task.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import Foundation

struct Task {
    let task: String
    let startingTaskDate: Date
    let finalTaskDate: Date
    let repetitionDate: [Date]
    let datesOfCompletion: [Date]
    let identifire: String
    let state: Bool
}

struct TodayTaskItem {
    let taskId: String
    let name: String
    let datesOfCompletion: [Date]
    let isSelected: Bool
}

struct TaskCompletionRecord {
    let taskId: String
    let dateOfCompletion: [Date]
}
