//
//  ScheduleDetailPresenter.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import Foundation

protocol ScheduleDetailViewProtocol: AnyObject {
    func success()
    func cdFailure()
    func deleteSuccess()
}

protocol ScheduleDetailViewPresenterProtocol: AnyObject {
    init(view: ScheduleDetailViewProtocol, model: Task, scheduleDetailProviderProtocol: ScheduleDetailProviderProtocol)
    
    var taskViewModel: ScheduleDetailViewModel { get }
    func viewDidLoad()
    func deleteData()
}

final class ScheduleDetailPresenter: ScheduleDetailViewPresenterProtocol {
    
    private let scheduleDetailModel: Task
    private let scheduleDetailProviderProtocol: ScheduleDetailProviderProtocol
    
    // MARK: - Properties
    
    weak var view: ScheduleDetailViewProtocol?
    
    var taskViewModel: ScheduleDetailViewModel {
        return ScheduleDetailViewModel(with: scheduleDetailModel)
    }
    
    // MARK: - Init
    
    required init(view: ScheduleDetailViewProtocol, model: Task, scheduleDetailProviderProtocol: ScheduleDetailProviderProtocol) {
        self.view = view
        self.scheduleDetailModel = model
        self.scheduleDetailProviderProtocol = scheduleDetailProviderProtocol
    }
    
    func viewDidLoad() {
        self.view?.success()
    }
    
    func deleteData() {
        scheduleDetailProviderProtocol.deleteNotification(with: taskViewModel.identifier)
        scheduleDetailProviderProtocol.deleteData(with: scheduleDetailModel) { result in
            switch result {
            case .failure(.savingFailure):
                self.view?.cdFailure()
            case .success(_):
                self.view?.deleteSuccess()
            }
        }
    }
}
