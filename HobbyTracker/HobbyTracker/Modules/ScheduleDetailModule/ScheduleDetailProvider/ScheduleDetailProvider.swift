//
//  ScheduleDetailProvider.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 24.02.2021.
//

import Foundation

protocol ScheduleDetailProviderProtocol {
    func deleteData(with model: Task, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func deleteNotification(with identifiers: String)
}

final class ScheduleDetailProvider: ScheduleDetailProviderProtocol {
    private let dataStorage: TaskStorageProtocol
    private let taskManager: TaskManagerProtocol
    private let notification: NotificationPublisherProtocol
    
    init(coreDataStorage: TaskStorageProtocol, taskManager: TaskManagerProtocol, notification: NotificationPublisherProtocol) {
        self.dataStorage = coreDataStorage
        self.taskManager = taskManager
        self.notification = notification
    }
    
    func deleteData(with model: Task, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        dataStorage.deleteData(delete: model, completionHandler: completionHandler)
    }
    
    func deleteNotification(with identifiers: String) {
        notification.deleteNotification(with: identifiers)
    }
}


