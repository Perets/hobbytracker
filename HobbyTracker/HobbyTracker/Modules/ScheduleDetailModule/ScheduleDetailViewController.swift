//
//  ScheduleDetailViewController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import UIKit

class ScheduleDetailViewController: UIViewController {

    // MARK: - Properties
    
    var scheduleDetailPresenter: ScheduleDetailViewPresenterProtocol!
    
    // MARK: - UI Components
    
    private var taskTextLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var dateLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var stateLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scheduleDetailPresenter.viewDidLoad()
        
        setupLayout()
        customize()
    }
    
    // MARK: - Customization
    
    private func customize() {
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteTapped))
    }

    // MARK: - Setup Layout
    
    private func setupLayout() {
        view.addSubview(taskTextLabel)
        taskTextLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        taskTextLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        taskTextLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        
        view.addSubview(dateLabel)
        dateLabel.topAnchor.constraint(equalTo: taskTextLabel.bottomAnchor, constant: 8).isActive = true
        dateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        dateLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        
        view.addSubview(stateLabel)
        stateLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 8).isActive = true
        stateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        stateLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
    }
    
    //MARK: Actions
    
    @objc
    func deleteTapped() {
        scheduleDetailPresenter.deleteData()
    }
    
    // MARK: - Setup Data
    
    private func setupData() {
        let modelTask = scheduleDetailPresenter.taskViewModel
        taskTextLabel.text = modelTask.taskText
        dateLabel.text = modelTask.startingTaskDate
        stateLabel.text = String(modelTask.state)
    }
}

extension ScheduleDetailViewController: ScheduleDetailViewProtocol {
    func success() {
        setupData()
    }
    
    func cdFailure() {
        
    }
    
    func deleteSuccess() {
        navigationController?.popViewController(animated: true)
    }
}
