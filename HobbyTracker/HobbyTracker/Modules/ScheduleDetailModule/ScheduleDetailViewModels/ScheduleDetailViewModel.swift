//
//  ScheduleDetailViewModel.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 26.02.2021.
//

import UIKit

struct ScheduleDetailViewModel {
    let task: Task
    
    let taskText: String
    let startingTaskDate: String
    let state: Bool
    let identifier: String
    
    init(with task: Task) {
        self.task = task
        
        self.taskText = task.task
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        self.startingTaskDate = "Task duration from \(dateFormatter.string(from: task.startingTaskDate))"
        self.state = task.state
        self.identifier = task.identifire
    }
}
