//
//  ScheduleDetailModuleFabric.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import Foundation

final class ScheduleDetailModuleFabric {
    private let dependencyContainer: DependencyContainer
    
    required init(with dependencyContainer: DependencyContainer) {
        self.dependencyContainer = dependencyContainer
    }
    
    func createScheduleDetailModule(scheduleModel: Task) -> ScheduleDetailViewController {
        let scheduleViewController = ScheduleDetailViewController()
        guard let scheduleDetailProvider = dependencyContainer.resolve(type: ScheduleDetailProvider.self) else { return scheduleViewController }
        
        let presenter = ScheduleDetailPresenter(view: scheduleViewController, model: scheduleModel, scheduleDetailProviderProtocol: scheduleDetailProvider)
        scheduleViewController.scheduleDetailPresenter = presenter
        
        return scheduleViewController
    }
}
