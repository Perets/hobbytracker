//
//  TodayPresenter.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import Foundation

protocol TodayViewProtocol: AnyObject {
    func success()
    func cdFailure()
}

protocol TodayViewPresenterProtocol: AnyObject {
    var numberOfTasks: Int { get }
    
    init(view: TodayViewProtocol, scheduleProvider: TodayProviderProtocol)
    
    func fetchData()
    func saveData(task: TodayTaskItem)
    func taskModel(at indexPath: IndexPath) -> TaskCellViewModel
}

class TodayPresenter: TodayViewPresenterProtocol {
    
    // MARK: - Private
    
    private let todayProvider: TodayProviderProtocol
    private var taskList = [TodayTaskItem]()
    
    //MARK: Properties
    
    weak var view: TodayViewProtocol?
    
    var numberOfTasks: Int {
        return taskList.count
    }
    
    //MARK: Init
    
    required init(view: TodayViewProtocol, scheduleProvider: TodayProviderProtocol) {
        self.view = view
        self.todayProvider = scheduleProvider
    }
    
    //MARK: Input methods
    
    func taskModel(at indexPath: IndexPath) -> TaskCellViewModel {
        
        return TaskCellViewModel(with: taskList[indexPath.row])
    }
    
    func fetchData() {
        todayProvider.fetchScheduleData { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success((let todayTask, let task)):
                self.taskList = todayTask
                self.view?.success()
                self.checkCompletion(task: task)
            case .failure(_):
                self.view?.cdFailure()
            }
        }
    }
    
    func saveData(task: TodayTaskItem) {
        var toggleState = task.isSelected
        toggleState.toggle()
        todayProvider.saveSelectedStateData(with: task, state: toggleState) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .failure(.savingFailure):
                self.view?.cdFailure()
            case .success(_):
                self.fetchData()
            }
        }
    }
    
    private func checkCompletion(task: [Task]) {
        todayProvider.checkCompleteState(task: task) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .failure(.savingFailure):
                self.view?.cdFailure()
            case .success(_):
                self.fetchData()
            }
        }
    }
}
