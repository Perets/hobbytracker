//
//  TodayProvider.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 26.02.2021.
//

import Foundation

protocol TodayProviderProtocol {
    func saveSelectedStateData(with model: TodayTaskItem, state: Bool, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func fetchScheduleData(completionHandler: @escaping (Result<([TodayTaskItem], [Task]), TaskManagerError>) -> Void)
    func checkCompleteState(task: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
}

final class TodayProvider: TodayProviderProtocol {
    private let taskManager: TaskManagerProtocol
    
    init(taskManager: TaskManagerProtocol) {
        self.taskManager = taskManager
    }
    
    func saveSelectedStateData(with model: TodayTaskItem, state: Bool, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        
        taskManager.saveSelectedStateData(with: model, state: state, completionHandler: completionHandler)
    }
    
    func fetchScheduleData(completionHandler: @escaping (Result<([TodayTaskItem], [Task]), TaskManagerError>) -> Void) {
        
        taskManager.fetchTasksForToday { todayTasks in
            switch todayTasks {
            case .success(_):
                completionHandler(todayTasks)
            case .failure(_):
                completionHandler(.failure(.failure))
            }
        }
    }
    
    func checkCompleteState(task: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        taskManager.checkCompleteState(tasks: task, completionHandler: completionHandler)
    }
}
