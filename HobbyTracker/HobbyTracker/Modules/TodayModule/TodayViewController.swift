//
//  TodayViewController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 13.02.2021.
//

import UIKit

class TodayViewController: UIViewController {

    //MARK: Properties
    
    var todayPresenter: TodayViewPresenterProtocol!
    
    // MARK: - UI Components
    
    private lazy var todayTasksTableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 64
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layoutMargins = .zero
        tableView.separatorInset = .zero
        
        tableView.dataSource = self
        tableView.delegate = self
       
        tableView.register(TaskTableViewCell.self, forCellReuseIdentifier: String(describing: TaskTableViewCell.self))
        
        return tableView
    }()
    
    // MARK: - Life cycle
    
    override func loadView() {
        super.loadView()
        
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        todayPresenter.fetchData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customize()
    }
    
    // MARK: - Customization
    
    func customize() {
        view.backgroundColor = .white
    }

    //MARK: Setup Layout
    
    private func setupLayout() {
        view.addSubview(todayTasksTableView)
        todayTasksTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        todayTasksTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        todayTasksTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        todayTasksTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

extension TodayViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todayPresenter.numberOfTasks
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TaskTableViewCell.self), for: indexPath) as! TaskTableViewCell
        
        cell.configure(with: todayPresenter.taskModel(at: indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = todayPresenter.taskModel(at: indexPath)
        todayPresenter.saveData(task: viewModel.task)
    }
}

extension TodayViewController: TodayViewProtocol {
    func success() {
        todayTasksTableView.reloadData()
    }
    
    func cdFailure() {
        print("Fail")
    }
}
