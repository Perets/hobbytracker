//
//  TodayTableViewCell.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    // MARK: - UI Components
    
    private let imageStateView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.isUserInteractionEnabled = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private let taskTextLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .systemFont(ofSize: 18.0)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        customize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Customize
    
    private func customize() {
        contentView.backgroundColor = .white
        self.layoutMargins = .zero
    }
    
    // MARK: - Setup Layout
    
    private func setupViews() {
        contentView.addSubview(imageStateView)
        imageStateView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        imageStateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        
        contentView.addSubview(taskTextLabel)
        taskTextLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 4).isActive = true
        taskTextLabel.leadingAnchor.constraint(equalTo: imageStateView.trailingAnchor, constant: 8).isActive = true
        taskTextLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        taskTextLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4).isActive = true
        taskTextLabel.centerYAnchor.constraint(equalTo: imageStateView.centerYAnchor).isActive = true
    }
    
    //MARK: Configure cell
    
    func configure(with model: TaskCellViewModel) {
        taskTextLabel.text = model.taskText
        imageStateView.image = model.image
    }
}
