//
//  TodayModuleFabric.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import UIKit

final class TodayModuleFabric {
    private let dependencyContainer: DependencyContainer
    
    required init(with dependencyContainer: DependencyContainer) {
        self.dependencyContainer = dependencyContainer
    }
    
    func createTodayModule() -> TodayViewController {
        let todayViewController = TodayViewController()
        guard let taskManager = dependencyContainer.resolve(type: TaskManager.self) else { return todayViewController }
        
        let provider = TodayProvider(taskManager: taskManager)
        let presenter = TodayPresenter(view: todayViewController, scheduleProvider: provider)
        todayViewController.todayPresenter = presenter
        
        todayViewController.tabBarItem.image = UIImage(named: "today_icon")
        todayViewController.title = "Today"
        
        return todayViewController
    }
}
