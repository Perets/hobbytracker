//
//  TaskCellViewModel.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 26.02.2021.
//

import UIKit

struct TaskCellViewModel {
    let task: TodayTaskItem
    
    var taskText: String
    var state: Bool
    var id: String
    var image: UIImage
    
    init(with task: TodayTaskItem) {
        self.task = task
        
        self.taskText = task.name
        self.id = task.taskId
        self.state = task.isSelected
        
        if task.isSelected {
            self.image = UIImage(named: "selectedCheckMark_icon") ?? UIImage()
            self.image = self.image.imageWithColor(color1: .green)
        } else {
            self.image = UIImage(named: "checkMark_icon") ?? UIImage()
            self.image = self.image.imageWithColor(color1: .orange)
        }
    }
}
