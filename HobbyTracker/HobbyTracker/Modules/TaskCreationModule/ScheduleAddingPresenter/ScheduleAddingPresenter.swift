//
//  ScheduleAddingPresenter.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 17.02.2021.
//

import UIKit
import UserNotifications

protocol ScheduleAddingViewProtocol: AnyObject {
    func dataLoaded(isDeclinedNotification: Bool)
    func notificationSaved()
    func cdFailure()
    func userHasDeclinedNotifications()
}

protocol ScheduleAddingViewPresenterProtocol: AnyObject {
    init(view: ScheduleAddingViewProtocol, scheduleProvider: ScheduleAddingProvider, modelArray: [Task])
    
    func viewDidLoad()
    func textViewDidBeginChange(textView: UITextView)
    func textViewDidChange(textView: UITextView)
    func startingDatePickerValueChanged(_ sender: UIDatePicker)
    func repetitionFrequencyDidChange(days: Int)
    func daysButtonTag(with tag: Int, state isSelected: Bool)
    func finalDatePickerValueChanged(_ sender: UIDatePicker)
    func reloadData()
    func sendNotification()
}

final class ScheduleAddingPresenter: ScheduleAddingViewPresenterProtocol {
    
    //MARK: Private
    
    private var taskList = [Task]()
    private var isDeclinedNotification = false
    private var taskModelForNotification: Task!
    
    private var scheduleText = ""
    private var startingScheduleDate = Date()
    private var finalScheduleDate = Date()
    
    private let scheduleAddingProvider: ScheduleAddingProvider
    private var daysArray: [Int] = []
    
    private var taskRepetition = 1
    private var hour = 0
    private var minute = 0
    private var weekday = 0
    
    private var id = ""
    
    //MARK: Properties
    
    weak var view: ScheduleAddingViewProtocol?
    
    //MARK: Init
    
    init(view: ScheduleAddingViewProtocol, scheduleProvider: ScheduleAddingProvider, modelArray: [Task]) {
        self.view = view
        self.scheduleAddingProvider = scheduleProvider
        self.taskList = modelArray
    }
    
    //MARK: Protocol methods
    
    func viewDidLoad() {
        scheduleAddingProvider.requestAuthorization { [weak self] result in
            switch result {
            case .failure(.savingFailure):
                self?.isDeclinedNotification = false
            case .success(_):
                self?.isDeclinedNotification = true
            }
        }
    }
    
    func reloadData() {
        id = convertedDate(from: Date())
        let datesBetweenArray = Date.dates(from: startingScheduleDate, to: finalScheduleDate, repetition: taskRepetition)
        guard let dayBefore = Calendar.current.date(byAdding: .day, value: -1, to: startingScheduleDate) else { return }
        
        let task = Task(task: scheduleText, startingTaskDate: startingScheduleDate, finalTaskDate: finalScheduleDate, repetitionDate: datesBetweenArray, datesOfCompletion: [dayBefore], identifire: id, state: false)
        
        saveData(task: task)
    }
    
    func textViewDidBeginChange(textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = .black
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        if textView.text?.count ?? 0 <= 100 {
            scheduleText = textView.text ?? ""
        }
        if textView.text?.count ?? 0 > 100 {
            textView.text = scheduleText
        }
    }
    
    func startingDatePickerValueChanged(_ sender: UIDatePicker) {
        startingScheduleDate = sender.date
    }
    
    func daysButtonTag(with tag: Int, state isSelected: Bool) {
        if isSelected {
            daysArray.append(tag)
        } else {
            daysArray.remove(object: tag)
        }
    }
    
    func finalDatePickerValueChanged(_ sender: UIDatePicker) {
        finalScheduleDate = sender.date
    }
    
    func repetitionFrequencyDidChange(days: Int) {
        if days == 4 {
            taskRepetition = 7
        } else {
            taskRepetition = days
        }
    }
    
    func sendNotification() {
        if isDeclinedNotification {
            view?.userHasDeclinedNotifications()
        }
        
        createCalendarDate()
        sendNotification(array: daysArray)
    }
    
    //MARK: Private methods
    
    private func convertedDate(from date: Date) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss E, dd.MM.yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    private func saveData(task: Task) {
        taskList.append(task)
        taskModelForNotification = task
        scheduleAddingProvider.scheduleFilling(model: taskList) { [weak self] result in
            switch result {
            case .failure(.savingFailure):
                self?.view?.cdFailure()
            case .success(_):
                self?.view?.dataLoaded(isDeclinedNotification: self?.isDeclinedNotification ?? false)
            }
        }
    }
    
    private func createCalendarDate() {
        let calendar = Calendar.current
        hour = calendar.component(.hour, from: startingScheduleDate)
        minute = calendar.component(.minute, from: startingScheduleDate)
        weekday = calendar.component(.weekday, from: startingScheduleDate)
    }
    
    private func sendNotification(array: [Int]) {
        if daysArray.isEmpty {
            daysArray.append(weekday)
        }
        
        //for day in daysArray {
            scheduleAddingProvider.scheduleNotificationEveryDay(identifier: id) { [weak self] result in
                switch result {
                case .failure(.savingFailure):
                    self?.view?.cdFailure()
                case .success(_):
                    self?.view?.notificationSaved()
                }
            }
        //}
    }
}

extension Array where Element: Equatable {
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else { return }
        remove(at: index)
    }
}
