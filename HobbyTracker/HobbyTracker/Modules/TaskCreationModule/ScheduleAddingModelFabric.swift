//
//  ScheduleAddingModelFabric.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import Foundation

final class ScheduleAddingModuleFabric {
    
    private let dependencyContainer: DependencyContainer
    
    required init(with dependencyContainer: DependencyContainer) {
        self.dependencyContainer = dependencyContainer
    }
    
    func createScheduleAddingModule(with taskArray: [Task]) -> ScheduleAddingViewController {
        let scheduleViewController = ScheduleAddingViewController()
        guard let scheduleAddingProvider = dependencyContainer.resolve(type: ScheduleAddingProvider.self) else { return scheduleViewController }
        
        let presenter = ScheduleAddingPresenter(view: scheduleViewController, scheduleProvider: scheduleAddingProvider, modelArray: taskArray)
        scheduleViewController.scheduleAddingPresenter = presenter
        
        return scheduleViewController
    }
}
