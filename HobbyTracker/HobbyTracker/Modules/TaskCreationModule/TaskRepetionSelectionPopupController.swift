//
//  TaskRepetionSelectionPopupController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.03.2021.
//

import UIKit

protocol ToolbarPickerViewDelegate: AnyObject {
    func didTapDone(text: String, perDay: Int)
    func didTapCancel()
}

class TaskRepetionSelectionPopupController: UIViewController {
    private enum Constants {
        static let cornerRadious: CGFloat = 15.0
        static let withAlphaComponent: CGFloat = 0.7
    }
    
    //MARK: - Private
    
    private var repeatArray = [""]
    
    //MARK: - Properties
    
    weak var toolbarDelegate: ToolbarPickerViewDelegate?
    
    // MARK: - UI Components
    
    private let mainView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = Constants.cornerRadious
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private let toolBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private let toolBarCancelButton: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
        
        return button
    }()
    
    private let toolBarDoneButton: UIButton = {
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(doneButtonAction), for: .touchUpInside)
        
        return button
    }()
    
    let contentContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var repetionPickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        
        return pickerView
    }()
    
    // MARK: - Init
    
    init(pickerViewList: [String]) {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        self.repeatArray = pickerViewList
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        swipeablePopupWindow = nil
    }
    
    // MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        
        setupViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customize()
    }
    
    // MARK: - Customize
    
    private func customize() {
        view.backgroundColor = UIColor.black.withAlphaComponent(Constants.withAlphaComponent)
    }
    
    // MARK: - Setup Views
    
    private func setupViews() {
        view.addSubview(mainView)
        mainView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mainView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true
        mainView.heightAnchor.constraint(equalTo: mainView.widthAnchor).isActive = true
        
        mainView.addSubview(toolBarView)
        toolBarView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        toolBarView.leftAnchor.constraint(equalTo: mainView.leftAnchor).isActive = true
        toolBarView.rightAnchor.constraint(equalTo: mainView.rightAnchor).isActive = true
        toolBarView.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.2).isActive = true
        
        toolBarView.addSubview(toolBarCancelButton)
        toolBarCancelButton.centerYAnchor.constraint(equalTo: toolBarView.centerYAnchor).isActive = true
        toolBarCancelButton.leftAnchor.constraint(equalTo: toolBarView.leftAnchor, constant: 16).isActive = true
        toolBarCancelButton.widthAnchor.constraint(equalTo: toolBarView.widthAnchor, multiplier: 0.4).isActive = true
        
        toolBarView.addSubview(toolBarDoneButton)
        toolBarDoneButton.centerYAnchor.constraint(equalTo: toolBarView.centerYAnchor).isActive = true
        toolBarDoneButton.rightAnchor.constraint(equalTo: toolBarView.rightAnchor, constant: -16).isActive = true
        toolBarDoneButton.widthAnchor.constraint(equalTo: toolBarView.widthAnchor, multiplier: 0.4).isActive = true
        
        mainView.addSubview(contentContainerView)
        contentContainerView.topAnchor.constraint(equalTo: toolBarView.bottomAnchor, constant: -12).isActive = true
        contentContainerView.leftAnchor.constraint(equalTo: mainView.leftAnchor).isActive = true
        contentContainerView.rightAnchor.constraint(equalTo: mainView.rightAnchor).isActive = true
        contentContainerView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -12).isActive = true
        
        contentContainerView.addSubview(repetionPickerView)
        repetionPickerView.topAnchor.constraint(equalTo: contentContainerView.topAnchor).isActive = true
        repetionPickerView.leftAnchor.constraint(equalTo: contentContainerView.leftAnchor).isActive = true
        repetionPickerView.rightAnchor.constraint(equalTo: contentContainerView.rightAnchor).isActive = true
        repetionPickerView.bottomAnchor.constraint(equalTo: contentContainerView.bottomAnchor).isActive = true
    }
    
    // MARK: - Actions
    
    @objc
    func cancelButtonAction() {
        toolbarDelegate?.didTapCancel()
        dismiss(animated: false, completion: nil)
    }
    
    @objc
    func doneButtonAction() {
        let row = repetionPickerView.selectedRow(inComponent: 0)
        repetionPickerView.selectRow(row, inComponent: 0, animated: false)
        toolbarDelegate?.didTapDone(text: repeatArray[row], perDay: row + 1)
        dismiss(animated: false, completion: nil)
    }
}

extension TaskRepetionSelectionPopupController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return repeatArray[row]
        }
}

extension TaskRepetionSelectionPopupController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return repeatArray.count
    }
}

fileprivate var swipeablePopupWindow: UIWindow?

extension TaskRepetionSelectionPopupController {
    func presentOnTop() {
        guard let windowScene = UIApplication.shared.connectedScenes.filter({ $0.activationState == .foregroundActive }).first as? UIWindowScene else {
            return
        }
        
        swipeablePopupWindow = UIWindow(windowScene: windowScene)
        swipeablePopupWindow?.rootViewController = UIViewController()
        
        swipeablePopupWindow?.windowLevel = .normal + 1
        swipeablePopupWindow?.makeKeyAndVisible()
        
        swipeablePopupWindow?.rootViewController?.present(self, animated: false)
    }
}
