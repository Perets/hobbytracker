//
//  ScheduleAddingViewController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 17.02.2021.
//

import UIKit

final class ScheduleAddingViewController: ScheduleAddingPopupController {
    
    //MARK: - Private
    
    private let repeateArrayList = ["- every day", "- every two days", "- every three days", "- every week"]
    
    //MARK: - Properties
    
    var scheduleAddingPresenter: ScheduleAddingViewPresenterProtocol!
    
    // MARK: - Transition closures
    
    var isSaved: ((_ saved: Bool) -> Void)?
    
    // MARK: - UI Components
    
    private lazy var scheduleTextView: UITextView = {
       let textView = UITextView()
        textView.isEditable = true
        textView.text = "Enter task here"
        textView.textColor = .lightGray
        textView.font = .systemFont(ofSize: 15)
        textView.autocorrectionType = .no
        textView.keyboardType = .default
        textView.isScrollEnabled = true
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textViewDidBeginEditing(_:)),
                                               name: UITextView.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChangeNotification(_:)),
                                               name: UITextView.textDidChangeNotification, object: nil)
        return textView
    }()
    
    private var startDatePickerLabel: UILabel = {
        let label = UILabel()
        label.text = "Start from"
        label.font = .boldSystemFont(ofSize: 15)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var finalDatePickerLabel: UILabel = {
        let label = UILabel()
        label.text = "Show until"
        label.font = .boldSystemFont(ofSize: 15)
        label.textAlignment = .right
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var startingDatePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.minimumDate = Date()
        picker.preferredDatePickerStyle = .compact
        picker.tintColor = .black
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        picker.addTarget(self, action: #selector(startingDatePickerValueChanged(_:)), for: .valueChanged)
        
        return picker
    }()
    
    private var finalDatePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.minimumDate = Date()
        picker.preferredDatePickerStyle = .compact
        picker.tintColor = .clear
        picker.isUserInteractionEnabled = false
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        picker.addTarget(self, action: #selector(finalDatePickerValueChanged(_:)), for: .valueChanged)
        
        return picker
    }()
    
    private var repeatLabel: UILabel = {
        let label = UILabel()
        label.text = "Repeat:"
        label.font = .boldSystemFont(ofSize: 15)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var repeatPickerTextField: UITextField = {
        let textField = UITextField()
        textField.text = "- every day"
        textField.font = .systemFont(ofSize: 15)
        textField.borderStyle = .roundedRect
        textField.autocorrectionType = .no
        textField.tintColor = .clear
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        textField.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        
        return textField
    }()
    
    private var customRuleLabel: UILabel = {
        let label = UILabel()
        label.text = "Custom rule:"
        label.font = .boldSystemFont(ofSize: 15)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var checkBoxButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "checkMark_icon"), for: .normal)
        button.setImage(UIImage(named: "selectedCheckMark_icon"), for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(checkBoxValueDidChange(_:)), for: .touchUpInside)
        
        return button
    }()
    
    private let daysContainerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 3.0
        stackView.isHidden = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private let mondayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Mon", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 2
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private let tuesdayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Tue", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 3
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private let wednesdayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Wed", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 4
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private let thursdayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Thu", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private let fridayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Fri", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 6
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private let saturdayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Sat", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 7
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private let sundayButton: UIButton = {
        let button = UIButton()
        button.setTitle("Sun", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        button.setTitleColor(.black, for: .normal)
        button.tag = 1
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.addTarget(self, action: #selector(daysButtonDidTap(with:)), for: .touchUpInside)
        
        return button
    }()
    
    private var payButton: UIButton = {
        let button = UIButton()
        button.setTitle("💾 Save", for: .normal)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 10
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    // MARK: - Setup Views
    
    override func setupContentContainer() {
        contentContainerView.addSubview(scheduleTextView)
        scheduleTextView.topAnchor.constraint(equalTo: contentContainerView.topAnchor, constant: 8).isActive = true
        scheduleTextView.leadingAnchor.constraint(equalTo: contentContainerView.leadingAnchor, constant: 20).isActive = true
        scheduleTextView.trailingAnchor.constraint(equalTo: contentContainerView.trailingAnchor, constant: -20).isActive = true
        scheduleTextView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        contentContainerView.addSubview(startDatePickerLabel)
        startDatePickerLabel.topAnchor.constraint(equalTo: scheduleTextView.bottomAnchor, constant: 8).isActive = true
        startDatePickerLabel.leadingAnchor.constraint(equalTo: scheduleTextView.leadingAnchor).isActive = true
        
        contentContainerView.addSubview(finalDatePickerLabel)
        finalDatePickerLabel.topAnchor.constraint(equalTo: scheduleTextView.bottomAnchor, constant: 8).isActive = true
        finalDatePickerLabel.trailingAnchor.constraint(equalTo: scheduleTextView.trailingAnchor).isActive = true
        
        contentContainerView.addSubview(startingDatePicker)
        startingDatePicker.topAnchor.constraint(equalTo: startDatePickerLabel.bottomAnchor, constant: 8).isActive = true
        startingDatePicker.leadingAnchor.constraint(equalTo: startDatePickerLabel.leadingAnchor).isActive = true
        
        contentContainerView.addSubview(finalDatePicker)
        finalDatePicker.topAnchor.constraint(equalTo: finalDatePickerLabel.bottomAnchor, constant: 8).isActive = true
        finalDatePicker.trailingAnchor.constraint(equalTo: finalDatePickerLabel.trailingAnchor).isActive = true
        
        contentContainerView.addSubview(repeatLabel)
        repeatLabel.topAnchor.constraint(equalTo: startingDatePicker.bottomAnchor, constant: 8).isActive = true
        repeatLabel.leadingAnchor.constraint(equalTo: startingDatePicker.leadingAnchor).isActive = true
        
        contentContainerView.addSubview(repeatPickerTextField)
        repeatPickerTextField.topAnchor.constraint(equalTo: repeatLabel.bottomAnchor, constant: 8).isActive = true
        repeatPickerTextField.leadingAnchor.constraint(equalTo: startingDatePicker.leadingAnchor).isActive = true
        repeatPickerTextField.trailingAnchor.constraint(equalTo: scheduleTextView.trailingAnchor).isActive = true
        
        contentContainerView.addSubview(customRuleLabel)
        customRuleLabel.topAnchor.constraint(equalTo: repeatPickerTextField.bottomAnchor, constant: 16).isActive = true
        customRuleLabel.leadingAnchor.constraint(equalTo: startingDatePicker.leadingAnchor).isActive = true
        
        contentContainerView.addSubview(checkBoxButton)
        checkBoxButton.leadingAnchor.constraint(equalTo: customRuleLabel.trailingAnchor, constant: 8).isActive = true
        checkBoxButton.centerYAnchor.constraint(equalTo: customRuleLabel.centerYAnchor).isActive = true
        
        contentContainerView.addSubview(daysContainerStackView)
        daysContainerStackView.topAnchor.constraint(equalTo: customRuleLabel.bottomAnchor, constant: 8).isActive = true
        daysContainerStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        daysContainerStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        daysContainerStackView.heightAnchor.constraint(equalTo: startingDatePicker.heightAnchor).isActive = true
        
        daysContainerSetup()
        
        contentContainerView.addSubview(payButton)
        payButton.topAnchor.constraint(equalTo: daysContainerStackView.bottomAnchor, constant: 16).isActive = true
        payButton.centerXAnchor.constraint(equalTo: contentContainerView.centerXAnchor).isActive = true
        payButton.widthAnchor.constraint(equalTo: contentContainerView.widthAnchor, multiplier: 0.6).isActive = true
        payButton.bottomAnchor.constraint(equalTo: contentContainerView.bottomAnchor, constant: -20).isActive = true
        
        payButton.addTarget(self, action: #selector(pressButton), for: .touchUpInside)
        
        scheduleAddingPresenter.viewDidLoad()
    }
    
    private func daysContainerSetup() {
        daysContainerStackView.addArrangedSubview(mondayButton)
        daysContainerStackView.addArrangedSubview(tuesdayButton)
        daysContainerStackView.addArrangedSubview(wednesdayButton)
        daysContainerStackView.addArrangedSubview(thursdayButton)
        daysContainerStackView.addArrangedSubview(fridayButton)
        daysContainerStackView.addArrangedSubview(saturdayButton)
        daysContainerStackView.addArrangedSubview(sundayButton)
    }
    
    override func deleteObserverForTextView() {
        NotificationCenter.default.removeObserver(scheduleTextView, name: UITextView.textDidBeginEditingNotification, object: nil)
    }
    
    //MARK: Actions
    
    @objc
    private func textViewDidBeginEditing(_ textView: UITextView) {
        scheduleAddingPresenter.textViewDidBeginChange(textView: scheduleTextView)
    }
    
    @objc
    private func textDidChangeNotification(_ textView: UITextView) {
        scheduleAddingPresenter.textViewDidChange(textView: scheduleTextView)
    }
    
    @objc
    private func startingDatePickerValueChanged(_ sender: UIDatePicker){
        scheduleAddingPresenter.startingDatePickerValueChanged(sender)
        scheduleAddingPresenter.finalDatePickerValueChanged(sender)
        finalDatePicker.isUserInteractionEnabled = true
        finalDatePicker.minimumDate = sender.date
        finalDatePicker.tintColor = .black

    }
    
    @objc
    private func finalDatePickerValueChanged(_ sender: UIDatePicker){
        scheduleAddingPresenter.finalDatePickerValueChanged(sender)
    }
    
    @objc
    func myTargetFunction(textField: UITextField) {
        let vc = TaskRepetionSelectionPopupController(pickerViewList: repeateArrayList)
        vc.presentOnTop()
        vc.toolbarDelegate = self
    }
    
    @objc
    func checkBoxValueDidChange(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            daysContainerStackView.isHidden = true
        } else {
            sender.isSelected = true
            daysContainerStackView.isHidden = false
        }
     }
    
    @objc
    private func daysButtonDidTap(with button: UIButton) {
        if button.backgroundColor == .white {
            button.backgroundColor = .black
            button.setTitleColor(.white, for: .normal)
            scheduleAddingPresenter.daysButtonTag(with: button.tag, state: true)
        } else if button.backgroundColor == .black {
            button.backgroundColor = .white
            button.setTitleColor(.black, for: .normal)
            scheduleAddingPresenter.daysButtonTag(with: button.tag, state: false)
        }
    }
    
    @objc
    private func pressButton() {
        view.endEditing(true)
        self.view.layoutIfNeeded()
        
        scheduleAddingPresenter.reloadData()
    }
}

extension ScheduleAddingViewController: ToolbarPickerViewDelegate {
    func didTapDone(text: String, perDay: Int) {
        repeatPickerTextField.text = text
        scheduleAddingPresenter.repetitionFrequencyDidChange(days: perDay)
        
    }
    
    func didTapCancel() {
        repeatPickerTextField.text = repeateArrayList.first
    }
}

extension ScheduleAddingViewController: ScheduleAddingViewProtocol {
    func dataLoaded(isDeclinedNotification: Bool) {
        isSaved?(true)
        presentAlertWithTitle(title: "Do you want to receive notifications", message: "", options: "Yes", "No") { [weak self] (options) in
            switch options {
            case 0:
                self?.scheduleAddingPresenter.sendNotification()
                if isDeclinedNotification {
                    self?.userHasDeclinedNotifications()
                } else {
                    self?.setupAnimated(heightConstant: self?.mainView.frame.height ?? 0, withAlphaComponent: 0.0, upAnimate: false)
                }
            case 1:
            
                self?.setupAnimated(heightConstant: self?.mainView.frame.height ?? 0, withAlphaComponent: 0.0, upAnimate: false)
            default:
                break
            }
        }
    }
    
    func cdFailure() {
        presentAlertWithTitle(title: "Data wasn't saved", message: "Please try again", options: "OK") { _ in }
    }
    
    func userHasDeclinedNotifications() {
        presentAlertWithTitle(title: "You didn't give permission to send notifications", message: "Please allow notifications", options: "OK") { [weak self] (options) in
            switch options {
            case 0:
                self?.setupAnimated(heightConstant: self?.mainView.frame.height ?? 0, withAlphaComponent: 0.0, upAnimate: false)
            default:
                break
            }
        }
    }
    
    func notificationSaved() {
        isSaved?(true)
    }
}
