//
//  ScheduleAddingPopupController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 17.02.2021.
//

import UIKit

class ScheduleAddingPopupController: UIViewController {
    private enum Constants {
        static let cornerRadious: CGFloat = 15.0
        static let withAlphaComponent: CGFloat = 0.7
        static let withOutAlphaComponent: CGFloat = 0.0
        static let duration: TimeInterval = 0.3
    }
    
    // MARK: - UI Components
    
    let mainView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var topView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var grayStreak: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .darkGray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let contentContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Properties
    
    private var isKeyboardShown = false
    private var bottomConstraint: NSLayoutConstraint?
    var canBeDismissed = true
    private var firstOpen = true
    
    // MARK: - Init
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {}
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissToTap)))
        mainView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(dragSPView)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstOpen {
            firstOpen = false
            
            bottomConstraint?.constant = mainView.frame.height
            view.layoutSubviews()
            
            setupAnimated(heightConstant: 0, withAlphaComponent: Constants.withAlphaComponent, upAnimate: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
        deleteObserverForTextView()
    }
    
    //MARK: - Keyboard method
    
    private func animateWithKeyboard(notification: NSNotification, animations: ((_ keyboardFrame: CGRect) -> Void)?) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        let durationKey = UIResponder.keyboardAnimationDurationUserInfoKey
        let duration = userInfo[durationKey] as! Double
        
        let frameKey = UIResponder.keyboardFrameEndUserInfoKey
        let keyboardFrameValue = userInfo[frameKey] as! NSValue
        
        let curveKey = UIResponder.keyboardAnimationCurveUserInfoKey
        let curveValue = userInfo[curveKey] as! Int
        let curve = UIView.AnimationCurve(rawValue: curveValue)!
        
        let animator = UIViewPropertyAnimator(
            duration: duration,
            curve: curve
        ) {
            animations?(keyboardFrameValue.cgRectValue)
            self.view?.layoutIfNeeded()
        }
        animator.startAnimation()
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard isKeyboardShown == false else { return }
        
        isKeyboardShown = true
        animateWithKeyboard(notification: notification) { (keyboardFrame) in
            self.bottomConstraint?.constant -= keyboardFrame.height
        }
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        guard isKeyboardShown == true else { return }
        
        animateWithKeyboard(notification: notification) { (keyboardFrame) in
            self.bottomConstraint?.constant += keyboardFrame.height
            self.isKeyboardShown = false
        }
    }
    
    // MARK: - Actions
    
    @objc
    func dismissToTap(_ sender: UITapGestureRecognizer? = nil) {
        guard isKeyboardShown == false else {
            self.view.endEditing(true)
            self.view.layoutIfNeeded()
            return
        }
        
        guard let gestureRecognizer = sender else { return }
        
        let view = gestureRecognizer.view
        let loc = gestureRecognizer.location(in: view)
        let hitView = view?.hitTest(loc, with: nil)
        
        guard hitView == self.view else { return }
        
        setupAnimated(heightConstant: self.mainView.frame.height, withAlphaComponent: Constants.withOutAlphaComponent, upAnimate: false)
    }
    
    // MARK: - Swipable view animate setup
    
    func setupAnimated(heightConstant: CGFloat, withAlphaComponent: CGFloat, upAnimate: Bool) {
        guard canBeDismissed == true else { return }
        
        UIView.animate(withDuration: Constants.duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: { [weak self] in
            guard let mySelf = self else { return }
            
            mySelf.bottomConstraint?.constant = heightConstant
            mySelf.view.backgroundColor = UIColor.black.withAlphaComponent(withAlphaComponent)
            mySelf.mainView.roundedView(width: 15, height: 15)
            mySelf.topView.roundedView(width: 15, height: 15)
            mySelf.view.layoutIfNeeded()
        }, completion: { _ in
            guard upAnimate == false else { return }
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    @objc
    private func dragSPView(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .changed:
            onChanged(recognizer: recognizer)
        case .ended:
            endedDrag(recognizer: recognizer)
        default:
            break
        }
    }
    
    private func onChanged(recognizer: UIPanGestureRecognizer) {
        let coorditane = recognizer.translation(in: view)
        
        guard isKeyboardShown == false else {
            self.view.endEditing(true)
            self.view.layoutIfNeeded()
            
            setupAnimated(heightConstant: 0, withAlphaComponent: Constants.withAlphaComponent, upAnimate: true)
            return
        }
        
        if coorditane.y > 0 {
            let newHeight = self.mainView.frame.height - coorditane.y
            let alphaComponentValue = 0.7 * (newHeight / self.mainView.frame.height)
            
            setupAnimated(heightConstant: coorditane.y, withAlphaComponent: alphaComponentValue, upAnimate: true)
        }
    }
    
    private func endedDrag(recognizer: UIPanGestureRecognizer) {
        let coorditane = recognizer.translation(in: view)
        if coorditane.y > (self.mainView.frame.height / 3) {
            setupAnimated(heightConstant: mainView.frame.height, withAlphaComponent: Constants.withOutAlphaComponent, upAnimate: false)
        } else {
            guard coorditane.y > 0 else { return }
            
            setupAnimated(heightConstant: 0, withAlphaComponent: Constants.withAlphaComponent, upAnimate: true)
        }
    }
    
    // MARK: - Setup Views
    
    private func setupViews() {
        view.addSubview(mainView)
        bottomConstraint = mainView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: UIScreen.main.bounds.height)
        bottomConstraint?.isActive = true
        mainView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mainView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        mainView.addSubview(topView)
        topView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: mainView.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: mainView.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        topView.addSubview(grayStreak)
        grayStreak.centerYAnchor.constraint(equalTo: topView.centerYAnchor).isActive = true
        grayStreak.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
        grayStreak.widthAnchor.constraint(equalToConstant: 70).isActive = true
        grayStreak.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        mainView.addSubview(contentContainerView)
        contentContainerView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0.5).isActive = true
        contentContainerView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -UIApplication.shared.safeAreaInsests.bottom).isActive = true
        contentContainerView.leftAnchor.constraint(equalTo: mainView.leftAnchor).isActive = true
        contentContainerView.rightAnchor.constraint(equalTo: mainView.rightAnchor).isActive = true
        contentContainerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        setupContentContainer()
    }
    
    // MARK: - Override for child
    
    func setupContentContainer() {}
    
    func deleteObserverForTextView() {}
}

extension UIApplication {
    var safeAreaInsests: UIEdgeInsets {
        guard let window = windows.first else {
            return .zero
        }
        
        return window.safeAreaInsets
    }
}
