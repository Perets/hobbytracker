//
//  ScheduleAddingProvider.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 22.02.2021.
//

import Foundation

protocol ScheduleAddingProviderProtocol {
    func scheduleFilling(model: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func requestAuthorization(completionHandler: @escaping (Result<Bool?, NotificationError>) -> Void)
    func scheduleNotificationEveryDay(identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func scheduleNotification(notificationBody: String, weekday: Int, hour: Int, minute: Int, identifire: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
}

final class ScheduleAddingProvider: ScheduleAddingProviderProtocol {
    private let taskManager: TaskManagerProtocol
    
    init(taskManager: TaskManagerProtocol) {
        self.taskManager = taskManager
    }
    
    func scheduleFilling(model: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        taskManager.scheduleSaveTask(model: model, completionHandler: completionHandler)
    }
    
    func requestAuthorization(completionHandler: @escaping (Result<Bool?, NotificationError>) -> Void) {
        taskManager.notificationRequestAuthorization(completionHandler: completionHandler)
    }
    
    func scheduleNotification(notificationBody: String, weekday: Int, hour: Int, minute: Int, identifire: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        taskManager.scheduleNotification(notificationBody: notificationBody, weekday: weekday, hour: hour, minute: minute, identifire: identifire, completionHandler: completionHandler)
    }
    
    func scheduleNotificationEveryDay(identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        taskManager.scheduleNotificationEveryDay(identifier: identifier, completionHandler: completionHandler)
    }
}
