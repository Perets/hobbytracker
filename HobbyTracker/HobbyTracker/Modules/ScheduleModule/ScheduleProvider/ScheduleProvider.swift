//
//  ScheduleProvider.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import Foundation

protocol ScheduleProviderProtocol {
    func fetchScheduleData(completionHandler: @escaping (Result<[Task], TaskManagerError>) -> Void)
    func scheduleNotification(notificationBody: String, delayInterval: Int?, identifier: String)
}

final class ScheduleProvider: ScheduleProviderProtocol {
    private let taskManager: TaskManagerProtocol
    
    init(taskManager: TaskManagerProtocol) {
        self.taskManager = taskManager
    }
    
    func fetchScheduleData(completionHandler: @escaping (Result<[Task], TaskManagerError>) -> Void) {
        taskManager.fetchAllTasksForManagement(completionHandler: completionHandler)
    }
    
    func scheduleNotification(notificationBody: String, delayInterval: Int?, identifier: String) {
        
    }
}
