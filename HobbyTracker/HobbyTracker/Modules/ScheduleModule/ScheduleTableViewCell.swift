//
//  ScheduleTableViewCell.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    
    // MARK: - UI Components
    
    let taskTextLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .systemFont(ofSize: 16.0)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let startingTaskDateLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let finalTaskDateLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    //MARK: Customize cell
    
    private func customize() {
        contentView.backgroundColor = .white
        self.layoutMargins = .zero
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup Layout
    
    private func setupViews() {
        contentView.addSubview(taskTextLabel)
        contentView.addSubview(startingTaskDateLabel)
        taskTextLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 4).isActive = true
        taskTextLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        taskTextLabel.trailingAnchor.constraint(equalTo: startingTaskDateLabel.leadingAnchor, constant: -8).isActive = true
        taskTextLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4).isActive = true
        
        startingTaskDateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        startingTaskDateLabel.widthAnchor.constraint(equalToConstant: 120).isActive = true
        startingTaskDateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        
        contentView.addSubview(finalTaskDateLabel)
        finalTaskDateLabel.widthAnchor.constraint(equalTo: startingTaskDateLabel.widthAnchor).isActive = true
        finalTaskDateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        finalTaskDateLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
    }
    
    //MARK: Configure cell
    
    func configure(with model: TaskViewModel) {
        taskTextLabel.text = model.taskText
        startingTaskDateLabel.text = model.startingTaskDate
        finalTaskDateLabel.text = model.finalTaskDate
    }

}
