//
//  ScheduleModuleFabric.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import UIKit

final class ScheduleModuleFabric {
    private let dependencyContainer: DependencyContainer
    
    required init(with dependencyContainer: DependencyContainer) {
        self.dependencyContainer = dependencyContainer
    }
    
    func createScheduleModule(isSaved: Bool) -> ScheduleViewController {
        let scheduleViewController = ScheduleViewController()
        guard let schedulerovider = dependencyContainer.resolve(type: ScheduleProvider.self) else { return scheduleViewController }
        
        let presenter = SchedulePresenter(view: scheduleViewController, scheduleProvider: schedulerovider)
        scheduleViewController.schedulePresenter = presenter
        if isSaved {
            scheduleViewController.schedulePresenter.reloadWithData()
        }
        scheduleViewController.tabBarItem.image = UIImage(named: "schedule_icon")
        scheduleViewController.title = "Schedule"
        
        return scheduleViewController
    }
}
