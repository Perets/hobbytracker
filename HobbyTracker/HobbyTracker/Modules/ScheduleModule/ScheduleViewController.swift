//
//  ScheduleViewController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 13.02.2021.
//

import UIKit

class ScheduleViewController: UIViewController {

    //MARK: Properties
    
    var schedulePresenter: ScheduleViewPresenterProtocol!
    
    // MARK: - Transition closures
    
    var scheduleAdding: ((_ task: [Task]) -> Void)?
    var scheduleSelect: ((_ task: Task) -> Void)?
    
    // MARK: - UI Components
    
    private lazy var scheduleTasksTableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 96
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layoutMargins = .zero
        tableView.separatorInset = .zero
        
        tableView.dataSource = self
        tableView.delegate = self
       
        tableView.register(ScheduleTableViewCell.self, forCellReuseIdentifier: String(describing: ScheduleTableViewCell.self))
        
        return tableView
    }()
    
    // MARK: - Life cycle
    
    override func loadView() {
        super.loadView()
        
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        schedulePresenter.fetchData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customize()
    }
    
    // MARK: - Customization
    
    func customize() {
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
    }
    
    // MARK: - Setup Layout
    
    private func setupLayout() {
        view.addSubview(scheduleTasksTableView)
        scheduleTasksTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scheduleTasksTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scheduleTasksTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scheduleTasksTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    //MARK: Actions
    
    @objc
    func addTapped() {
        scheduleAdding?(schedulePresenter.taskArray)
    }
}

extension ScheduleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedulePresenter.numberOfTasks
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ScheduleTableViewCell.self), for: indexPath) as! ScheduleTableViewCell
        
        cell.configure(with: schedulePresenter.taskModel(at: indexPath))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = schedulePresenter.taskModel(at: indexPath)
        
        scheduleSelect?(viewModel.task)
    }
}

extension ScheduleViewController: ScheduleViewProtocol {
    func cdFailure() {
        print("Core Data Failure")
    }
    
    func success() {
        scheduleTasksTableView.reloadData()
    }
}
