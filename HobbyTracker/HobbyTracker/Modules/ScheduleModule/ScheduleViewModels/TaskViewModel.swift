//
//  ScheduleTableViewCellModel.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import UIKit

struct TaskViewModel {
    let task: Task
    
    let taskText: String
    let startingTaskDate: String
    let finalTaskDate: String
    let notificationIdentifier: String
    let state: Bool
    var image: UIImage
    
    init(with task: Task) {
        self.task = task
        self.taskText = task.task
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        self.startingTaskDate = "Start schedule date: \(dateFormatter.string(from: task.startingTaskDate))"
        self.finalTaskDate = "Final schedule date: \(dateFormatter.string(from: task.finalTaskDate))"
        self.notificationIdentifier = ""
        
        self.state = task.state
        
        if task.state {
            self.image = UIImage(named: "selectedCheckMark_icon") ?? UIImage()
            self.image = self.image.imageWithColor(color1: .green)
        } else {
            self.image = UIImage(named: "checkMark_icon") ?? UIImage()
            self.image = self.image.imageWithColor(color1: .orange)
        }
    }
}
