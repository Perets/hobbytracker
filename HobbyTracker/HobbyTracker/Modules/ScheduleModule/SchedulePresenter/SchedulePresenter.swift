//
//  SchedulePresenter.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import Foundation

protocol ScheduleViewProtocol: AnyObject {
    func success()
    func cdFailure()
}

protocol ScheduleViewPresenterProtocol: AnyObject {
    var numberOfTasks: Int { get }
    var taskArray: [Task] { get }
    
    init(view: ScheduleViewProtocol, scheduleProvider: ScheduleProviderProtocol)
    func fetchData()
    func taskModel(at indexPath: IndexPath) -> TaskViewModel
    func reloadWithData()
}

class SchedulePresenter: ScheduleViewPresenterProtocol {
    
    // MARK: - Private
    
    private let scheduleProvider: ScheduleProviderProtocol
    var taskList = [Task]()
    
    // MARK: - Properties
    
    weak var view: ScheduleViewProtocol?
    
    var taskText: Task?
    
    var numberOfTasks: Int {
        return taskList.count
    }
    
    var taskArray: [Task] {
        return taskList
    }
    
    //MARK: Init
    
    required init(view: ScheduleViewProtocol, scheduleProvider: ScheduleProviderProtocol) {
        self.view = view
        self.scheduleProvider = scheduleProvider
    }
    
    //MARK: Input methods
    
    func taskModel(at indexPath: IndexPath) -> TaskViewModel {
        let sortedArray = taskList.sorted {($0.startingTaskDate < $1.startingTaskDate)}
        
        return TaskViewModel(with: sortedArray[indexPath.row])
    }
    
    func reloadWithData() {
        view?.success()
    }
    
    func fetchData() {
        scheduleProvider.fetchScheduleData { result in
            switch result {
            case .success(let task):
                self.taskList = task
                self.view?.success()
            case .failure(_):
                print("error")
            }
        }
    }
}
