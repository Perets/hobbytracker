//
//  MainTabBarViewController.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 12.02.2021.
//

import UIKit

final class MainTabBarViewController: UITabBarController {

    // MARK: - TabBar properties
    
    private let dependencyContainer: DependencyContainer
    
    private var todayFlowCoordinator: TodayCoordinator?
    private var scheduleFlowCoordinator: ScheduleCoordinator?
    
    //MARK: Init
    
    init(with dependencyContainer: DependencyContainer) {
        self.dependencyContainer = dependencyContainer
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Life Cycle
    
    override func loadView() {
        super.loadView()
        
        createItems()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        customize()
    }
    
    // MARK: Customize controller
    
    private func customize() {
        view.backgroundColor = .white
    }
    
    // MARK: - TabBarController part
    
    private func createItems() {
        let todayNavigationController = createTodayTab()
        
        let scheduleNavigationController = createScheduleTab()
        
        let controllers = [todayNavigationController, scheduleNavigationController]
        self.viewControllers = controllers
    }
    
    private func createTodayTab() -> UINavigationController {
        let todayNavigationController = UINavigationController()
        todayFlowCoordinator = TodayCoordinator(with: todayNavigationController, dependencyContainer: dependencyContainer)
        todayFlowCoordinator?.start()
        
        return todayNavigationController
    }
    
    private func createScheduleTab() -> UINavigationController {
        let scheduleNavigationController = UINavigationController()
        scheduleFlowCoordinator = ScheduleCoordinator(with: scheduleNavigationController, dependencyContainer: dependencyContainer)
        scheduleFlowCoordinator?.start()
        return scheduleNavigationController
    }
}

// MARK: - UITabBarControllerDelegate

extension MainTabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
}
