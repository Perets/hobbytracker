//
//  TaskManagerProtocol.swift
//  HobbyTracker
//
//  Created by Aleksandr Honcharov on 10.03.2021.
//

import Foundation

enum TaskManagerError: Error {
    case failure
    case storageError
}

protocol TaskManagerProtocol {
    var storage: TaskStorageProtocol { get }
    var notificationPublisher: NotificationPublisherProtocol { get }
    
    func fetchTasksForToday(completionHandler: @escaping (Result<([TodayTaskItem], [Task]), TaskManagerError>) -> Void)
    func checkCompleteState(tasks: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func fetchAllTasksForManagement(completionHandler: @escaping (Result<[Task], TaskManagerError>) -> Void)
    func saveSelectedStateData(with model: TodayTaskItem, state: Bool,
                               completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func scheduleSaveTask(model: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    
    func fetchAllCompletedTask(completionHandler: @escaping (Result<[TaskCompletionRecord], TaskManagerError>) -> Void)
    
    func notificationRequestAuthorization(completionHandler: @escaping (Result<Bool?, NotificationError>) -> Void)
    func scheduleNotificationEveryDay(identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func scheduleNotification(notificationBody: String, weekday: Int, hour: Int, minute: Int, identifire: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
}
