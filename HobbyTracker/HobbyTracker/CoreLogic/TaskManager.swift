//
//  TaskManager.swift
//  HobbyTracker
//
//  Created by Aleksandr Honcharov on 10.03.2021.
//

import Foundation

final class TaskManager: TaskManagerProtocol {
    
    // MARK: Properties
    
    var storage: TaskStorageProtocol
    var notificationPublisher: NotificationPublisherProtocol
    
    // MARK: Init
    
    init(storage: TaskStorageProtocol, publisher: NotificationPublisherProtocol) {
        self.storage = storage
        self.notificationPublisher = publisher
    }
    
    func fetchTasksForToday(completionHandler: @escaping (Result<([TodayTaskItem], [Task]), TaskManagerError>) -> Void) {
        var todayTasks = [TodayTaskItem]()
        
        if let tasks = storage.fetchTask() {
            let filteredTasks = filteredArray(with: tasks)
            filteredTasks.forEach { task in
                let todayTask = TodayTaskItem(taskId: task.identifire, name: task.task, datesOfCompletion: task.datesOfCompletion, isSelected: task.state)
                todayTasks.append(todayTask)
                completionHandler(.success((todayTasks, tasks)))
            }
        } else {
            completionHandler(.failure(.failure))
        }
    }
    
    func checkCompleteState(tasks: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        tasks.forEach { task in
            guard task.state == true else { return }
            
            let result = task.datesOfCompletion.contains { completedDate in
                Calendar.current.isDateInToday(completedDate)
            }
            if !result {
                var newState = task.state
                newState.toggle()
                storage.saveTaskWithSelectedState(save: task, completionDates: task.datesOfCompletion, state: newState, completionHandler: completionHandler)
            }
        }
    }
    
    func fetchAllTasksForManagement(completionHandler: @escaping (Result<[Task], TaskManagerError>) -> Void) {
        if let task = storage.fetchTask() {
            completionHandler(.success(task))
        } else {
            completionHandler(.failure(.storageError))
        }
    }
    
    func fetchAllCompletedTask(completionHandler: @escaping (Result<[TaskCompletionRecord], TaskManagerError>) -> Void) {
        
    }
    
    func scheduleSaveTask(model: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        storage.saveTask(model, completionHandler: completionHandler)
    }
    
    func saveSelectedStateData(with model: TodayTaskItem, state: Bool, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        
        let task = Task(task: model.name,
                        startingTaskDate: Date(),
                        finalTaskDate: Date(),
                        repetitionDate: [Date](),
                        datesOfCompletion: model.datesOfCompletion,
                        identifire: model.taskId,
                        state: model.isSelected)
        
        var completionDateArray = model.datesOfCompletion
        
        for completionDate in completionDateArray {
            let result = completionDateArray.contains { completedDate in
                Calendar.current.isDateInToday(completedDate)
            }
            if model.isSelected && Calendar.current.isDateInToday(completionDate) {
                completionDateArray.remove(object: completionDate)
            } else if !model.isSelected && !result {
                completionDateArray.append(Date())
            }
        }
        
        storage.saveTaskWithSelectedState(save: task, completionDates: completionDateArray, state: state, completionHandler: completionHandler)
    }
    
    // MARK: Notification
    
    func notificationRequestAuthorization(completionHandler: @escaping (Result<Bool?, NotificationError>) -> Void) {
        notificationPublisher.requestAuthorization(completionHandler: completionHandler)
    }
    
    func scheduleNotificationEveryDay(identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        notificationPublisher.scheduleNotificationEveryDay(identifier: identifier, completionHandler: completionHandler)
    }
    
    func scheduleNotification(notificationBody: String, weekday: Int, hour: Int, minute: Int, identifire: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {        notificationPublisher.scheduleNotificationWithCalenderThroughoutDates(notificationBody: notificationBody, weekday: weekday, hour: hour, minute: minute, identifier: identifire, completionHandler: completionHandler)
    }
    
    // MARK: Private Methods
    
    private func filteredArray(with taskArray: [Task]) -> [Task] {
        var sortedTaskArray = [Task]()
        for task in taskArray {
            if let todayTask = todayDate(task: task) {
                sortedTaskArray.append(todayTask)
            }
        }
        
        let sortedArray = sortedTaskArray.sorted {($0.startingTaskDate < $1.startingTaskDate)}
        
        return sortedArray
    }
    
    private func convertedDate(from date: Date) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    private func todayDate(task: Task) -> Task? {
        for date in task.repetitionDate {
            if convertedDate(from: Date()) == convertedDate(from: date) {
                return task
            }
        }
        return nil
    }
}
