//
//  SceneDelegate.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 12.02.2021.
//

import UIKit
import CoreData

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    private var dependencyContainer: DependencyContainer!
    
    let notifications = NotificationPublisher()
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        dependencyContainer = DependencyContainer.shared
        dependencyContainer.register(type: CoreDataStorage.self, service: CoreDataStorage())
        
        guard let cdStorage = dependencyContainer.resolve(type: CoreDataStorage.self) else { return }
        let notificationPublisher = NotificationPublisher()
        let taskManager = TaskManager(storage: cdStorage, publisher: notificationPublisher)
        dependencyContainer.register(type: TaskManager.self, service: taskManager)
        
        dependencyContainer.register(type: ScheduleProvider.self, service: ScheduleProvider(taskManager: taskManager))
        
        dependencyContainer.register(type: ScheduleAddingProvider.self, service: ScheduleAddingProvider(taskManager: taskManager))
        
        dependencyContainer.register(type: ScheduleDetailProvider.self, service: ScheduleDetailProvider(coreDataStorage: cdStorage, taskManager: taskManager, notification: notificationPublisher))
        
        notifications.notificationCenter.delegate = notifications
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = MainTabBarViewController(with: dependencyContainer)
        window?.makeKeyAndVisible()
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}
