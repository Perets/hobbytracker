//
//  Date+DateRepetition.swift
//  HobbyTracker
//
//  Created by Igor Perets on 27.10.2021.
//

import UIKit

extension Date {
    static func dates(from fromDate: Date, to toDate: Date, repetition: Int) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        dates.append(date)
        
        while date <= toDate {
            
            guard let newDate = Calendar.current.date(byAdding: .day, value: repetition, to: date) else { break }
            date = newDate
            dates.append(date)
        }
        return dates
    }
}
