//
//  UIView+RoundedView.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 26.02.2021.
//

import UIKit

extension UIView {
    func roundedView(width: CGFloat, height: CGFloat) {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii: CGSize(width: width, height: width))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
