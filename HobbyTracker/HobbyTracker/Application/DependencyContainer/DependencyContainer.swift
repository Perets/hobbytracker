//
//  DependencyContainer.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 22.02.2021.
//

import Foundation
import CoreData

class DependencyContainer {
    static let shared = DependencyContainer()
    
    private init() {}
    
    private var dependecies: [DependencyKey : Any] = [:]
    
    func register<T>(type: T.Type, name: String? = nil, service: Any) {
        let dependencyKey = DependencyKey(type: type, name: name)
        dependecies[dependencyKey] = service
    }
    
    func resolve<T>(type: T.Type, name: String? = nil) -> T? {
        let dependencyKey = DependencyKey(type: type, name: name)
        return dependecies[dependencyKey] as? T
    }
}
