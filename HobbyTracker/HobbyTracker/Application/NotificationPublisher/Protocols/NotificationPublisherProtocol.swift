//
//  NotificationPublisherProtocol.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 02.03.2021.
//

import Foundation

protocol NotificationPublisherProtocol {
    func requestAuthorization(completionHandler: @escaping (Result<Bool?, NotificationError>) -> Void)
    func scheduleNotificationEveryDay(identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func scheduleNotificationWithCalenderThroughoutDates(notificationBody: String, weekday: Int, hour: Int, minute: Int, identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func deleteNotification(with identifiers: String)
}
