//
//  NotificationPublisher.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 27.02.2021.
//

import UIKit
import UserNotifications

enum NotificationError: Error {
  case savingFailure
}

class NotificationPublisher: NSObject, UNUserNotificationCenterDelegate, NotificationPublisherProtocol {
    
    let notificationCenter = UNUserNotificationCenter.current()
    let content = UNMutableNotificationContent()
    
    func requestAuthorization(completionHandler: @escaping (Result<Bool?, NotificationError>) -> Void) {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] (didAllow, error) in
            guard didAllow else { completionHandler(.success(true))
                return
            }
            self?.notificationCenter.getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { completionHandler(.success(true))
                    return
                }
            }
        }
    }
    
    func scheduleNotificationEveryDay(identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "You have some task to do"
        notificationContent.body = "To do them now"
        notificationContent.badge = NSNumber(value: 1)
        notificationContent.sound = .default
        
        var datComp = DateComponents()
        datComp.hour = 12
        datComp.minute = 00
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: datComp, repeats: true)
        let request = UNNotificationRequest(identifier: identifier, content: notificationContent, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error : Error?) in
            if let theError = error {
                completionHandler(.failure(.savingFailure))
                print(theError.localizedDescription)
            }
        }
        
        completionHandler(.success(true))
    }
    
    func scheduleNotificationWithCalenderThroughoutDates(notificationBody: String, weekday: Int, hour: Int, minute: Int, identifier: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        let userActions = "User Actions"
        content.title = "You have one task to do"
        content.subtitle = "Try to do it now"
        content.body = notificationBody
        content.sound = .default
        content.badge = 1
        
        var dateComponents = DateComponents()
        dateComponents.weekday = weekday
        dateComponents.hour = hour
        dateComponents.minute = minute + 2
        
        let calendarNotificationTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: calendarNotificationTrigger)
        notificationCenter.add(request) { (error) in
            if let error = error {
                completionHandler(.failure(.savingFailure))
                print("Error \(error.localizedDescription)")
            }
        }
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: userActions,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
        completionHandler(.success(true))
    }
    
    func deleteNotification(with identifier: String) {
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    //MARK: UNUserNotificationCenterDelegate
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.banner,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
        case "Delete":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }
}
