//
//  ScheduleCoordinatorProtocol.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 21.02.2021.
//

import UIKit

protocol ScheduleCoordinatorProtocol {
    var navigationController: UINavigationController { get }
    var dependencyContainer: DependencyContainer { get }
    
    init(with navigationController: UINavigationController, dependencyContainer: DependencyContainer)
    
    func start()
}
