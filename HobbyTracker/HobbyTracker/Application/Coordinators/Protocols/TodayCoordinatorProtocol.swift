//
//  TodayCoordinatorProtocol.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import UIKit

protocol TodayCoordinatorProtocol {
    var navigationController: UINavigationController { get }
    var dependencyContainer: DependencyContainer { get }
    
    init(with navigationController: UINavigationController, dependencyContainer: DependencyContainer)
    
    func start()
}
