//
//  TodayCoordinator.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import UIKit

final class TodayCoordinator: TodayCoordinatorProtocol {
    let navigationController: UINavigationController
    let dependencyContainer: DependencyContainer
    
    init(with navigationController: UINavigationController, dependencyContainer: DependencyContainer) {
        self.navigationController = navigationController
        self.dependencyContainer = dependencyContainer
    }
    
    func start() {
        todayPage()
    }
    
    private func todayPage() {
        let todayModuleFabric = TodayModuleFabric(with: dependencyContainer)
        let todayController = todayModuleFabric.createTodayModule()
        
        navigationController.setViewControllers([todayController], animated: false)
    }
    
}
