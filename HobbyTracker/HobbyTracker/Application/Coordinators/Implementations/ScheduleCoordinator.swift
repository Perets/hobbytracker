//
//  ScheduleCoordinator.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 19.02.2021.
//

import UIKit

final class ScheduleCoordinator: ScheduleCoordinatorProtocol {
    let navigationController: UINavigationController
    let dependencyContainer: DependencyContainer
    
    init(with navigationController: UINavigationController, dependencyContainer: DependencyContainer) {
        self.navigationController = navigationController
        self.dependencyContainer = dependencyContainer
    }
    
    func start() {
        schedulePage()
    }
    
    private func schedulePage(isSaved: Bool = false) {
        let scheduleModuleFabric = ScheduleModuleFabric(with: dependencyContainer)
        let scheduleController = scheduleModuleFabric.createScheduleModule(isSaved: isSaved)
        
        scheduleController.scheduleAdding = { [weak self] task in
            self?.scheduleAddingPage(with: task)
        }
        
        scheduleController.scheduleSelect = { [weak self] task in
            self?.scheduleDetailPage(with: task)
        }
        
        navigationController.setViewControllers([scheduleController], animated: false)
    }
    
    private func scheduleAddingPage(with taskArray: [Task]) {
        let scheduleAddingModuleFabric = ScheduleAddingModuleFabric(with: dependencyContainer)
        let addingController = scheduleAddingModuleFabric.createScheduleAddingModule(with: taskArray)
        
        addingController.isSaved = { [weak self] saved in
            self?.schedulePage(isSaved: saved)
        }
        
        navigationController.present(addingController, animated: false, completion: nil)
    }
    
    private func scheduleDetailPage(with task: Task) {
        let scheduleDetailModuleFabric = ScheduleDetailModuleFabric(with: dependencyContainer)
        let detailController = scheduleDetailModuleFabric.createScheduleDetailModule(scheduleModel: task)
        
        navigationController.pushViewController(detailController, animated: true)
    }
}
