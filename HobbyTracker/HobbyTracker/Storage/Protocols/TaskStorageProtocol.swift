//
//  UserStorageProtocol.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import Foundation

protocol TaskStorageProtocol {
    func saveTask(_ task: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func saveTaskWithSelectedState(save task: Task, completionDates: [Date], state: Bool, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func fetchTask() -> [Task]?
    func clearStorage(_ entity: String)
    func deleteData(delete task: Task, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    
    func saveTaskCompletion(_ task: [TaskCompletionRecord], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
    func fetchTaskCompletion() -> [TaskCompletionRecord]?
    func deleteTaskCompletionData(delete task: TaskCompletionRecord, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void)
}
