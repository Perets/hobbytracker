//
//  CoreDataTask.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import Foundation
import CoreData

extension CoreDataTask {
    func createTask() -> Task {
        let task = Task(task: self.taskText ?? "",
                        startingTaskDate: self.startingTaskDate ?? Date(),
                        finalTaskDate: self.finalTaskDate ?? Date(),
                        repetitionDate: self.repetitionDate ?? [Date](),
                        datesOfCompletion: self.datesOfCompletion ?? [Date](),
                        identifire: self.identifire ?? "",
                        state: self.state)
        return task
    }
    
    static func makeFrom(task: Task, context: NSManagedObjectContext) -> CoreDataTask {
        let cdTask = CoreDataTask(context: context)
        cdTask.taskText = task.task
        cdTask.startingTaskDate = task.startingTaskDate
        cdTask.finalTaskDate = task.finalTaskDate
        cdTask.repetitionDate = task.repetitionDate
        cdTask.datesOfCompletion = task.datesOfCompletion
        cdTask.identifire = task.identifire
        cdTask.state = task.state

        return cdTask
    }
    
    static func fetch(context: NSManagedObjectContext) -> Task? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataEntityName.coreDataTask)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let task = data.value(forKey: CoreDataPropertiesName.taskText) as! String
                let startingTaskDate = data.value(forKey: CoreDataPropertiesName.startingTaskDate) as! Date
                let finalTaskDate = data.value(forKey: CoreDataPropertiesName.finalTaskDate) as! Date
                let repetitionDate = data.value(forKey: CoreDataPropertiesName.repetitionDate) as! [Date]
                let datesOfCompletion = data.value(forKey: CoreDataPropertiesName.repetitionDate) as! [Date]
                let state = data.value(forKey: CoreDataPropertiesName.state) as! Bool
                let identifire = data.value(forKey: CoreDataPropertiesName.identifire) as! String
                let model = Task(task: task, startingTaskDate: startingTaskDate, finalTaskDate: finalTaskDate, repetitionDate: repetitionDate, datesOfCompletion: datesOfCompletion, identifire: identifire, state: state)
                
                return model
            }
        } catch {
            return nil
        }
        return nil
    }
}
