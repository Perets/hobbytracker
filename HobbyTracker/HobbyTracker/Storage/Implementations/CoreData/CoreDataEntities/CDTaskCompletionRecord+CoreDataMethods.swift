//
//  CDTaskCompletionRecord+CoreDataMethods.swift
//  HobbyTracker
//
//  Created by Igor Perets on 30.10.2021.
//

import Foundation
import CoreData

extension CDTaskCompletionRecord {
    func createCDTaskCompletion() -> TaskCompletionRecord {
        let task = TaskCompletionRecord(taskId: self.taskId ?? "", dateOfCompletion: self.dateOfCompletion ?? [Date]())
        
        return task
    }
    
    static func makeFrom(task: TaskCompletionRecord, context: NSManagedObjectContext) -> CDTaskCompletionRecord {
        let cdTask = CDTaskCompletionRecord(context: context)
        cdTask.taskId = task.taskId
        cdTask.dateOfCompletion = task.dateOfCompletion
        
        return cdTask
    }
    
    static func fetch(context: NSManagedObjectContext) -> TaskCompletionRecord? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataEntityName.taskCompletionRecord)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let taskId = data.value(forKey: CDTaskCompletionProperties.taskId) as! String
                let dateOfCompletion = data.value(forKey: CDTaskCompletionProperties.dateOfCompletion) as! [Date]
                let model = TaskCompletionRecord(taskId: taskId, dateOfCompletion: dateOfCompletion)
                
                return model
            }
        } catch {
            return nil
        }
        return nil
    }
}
