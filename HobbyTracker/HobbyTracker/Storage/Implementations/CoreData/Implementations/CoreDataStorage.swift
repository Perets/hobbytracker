//
//  CoreDataStorage.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 15.02.2021.
//

import UIKit
import CoreData

enum StorageError: Error {
    case savingFailure
}

final class CoreDataStorage: TaskStorageProtocol {
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "HobbyTracker")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - UserCoreData
    
    func saveTask(_ task: [Task], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        clearStorage(CoreDataEntityName.coreDataTask)
        
        let context = persistentContainer.viewContext
        task.forEach { CoreDataTask.makeFrom(task: $0, context: context) }
        
        saving(context: context, completionHandler: completionHandler)
    }
    
    func fetchTask() -> [Task]? {
        guard let fetchedRecords = fetchManagedObject(managedObject: CoreDataTask.self) else {
            return nil
        }
        
        let tasks = fetchedRecords.map { $0.createTask() }
        return tasks
    }
    
    func saveTaskWithSelectedState(save task: Task, completionDates: [Date], state: Bool, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        
        updateCoreDataValue(in: task, completionDates: completionDates, updateValue: state, for: CoreDataPropertiesName.state, completionHandler: completionHandler)
    }
    
    func deleteData(delete task: Task, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        let context = persistentContainer.viewContext
        let request = searchTask(in: task)
        
        let result = try? context.fetch(request)
        let resultData = result as! [NSManagedObject]
        for object in resultData {
            context.delete(object)
        }
        
        saving(context: context, completionHandler: completionHandler)
    }
}

// MARK: - TaskCompletionRecord

extension CoreDataStorage {
    func saveTaskCompletion(_ task: [TaskCompletionRecord], completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        clearStorage(CoreDataEntityName.taskCompletionRecord)
        
        let context = persistentContainer.viewContext
        task.forEach { CDTaskCompletionRecord.makeFrom(task: $0, context: context) }
        
        saving(context: context, completionHandler: completionHandler)
    }
    
    func fetchTaskCompletion() -> [TaskCompletionRecord]? {
        guard let fetchedRecords = fetchManagedObject(managedObject: CDTaskCompletionRecord.self) else {
            return nil
        }
        
        let tasks = fetchedRecords.map { $0.createCDTaskCompletion() }
        
        return tasks
    }
    
    func deleteTaskCompletionData(delete task: TaskCompletionRecord, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        let context = persistentContainer.viewContext
        let request = searchCompletionTask(in: task)
        
        let result = try? context.fetch(request)
        let resultData = result as! [NSManagedObject]
        for object in resultData {
            context.delete(object)
        }
        
        saving(context: context, completionHandler: completionHandler)
    }
}

extension CoreDataStorage {
    private func saving(context: NSManagedObjectContext, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        do {
            try context.save()
            completionHandler(.success(true))
        } catch {
            completionHandler(.failure(.savingFailure))
        }
    }
    
    private func updateCoreDataValue<T>(in task: Task, completionDates: [Date], updateValue: T, for key: String, completionHandler: @escaping (Result<Bool?, StorageError>) -> Void) {
        let context = persistentContainer.viewContext
        let request = searchTask(in: task)
        
        do {
            let results = try context.fetch(request) as? [NSManagedObject]
            if results?.count != 0 {
                results?[0].setValue(updateValue, forKey: key)
                results?[0].setValue(completionDates, forKey: "datesOfCompletion")
            }
        } catch {
            completionHandler(.failure(.savingFailure))
        }
        saving(context: context, completionHandler: completionHandler)
    }
    
    private func searchTask(in task: Task) -> NSFetchRequest<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataEntityName.coreDataTask)
        request.predicate = NSPredicate(format: "\(CoreDataPropertiesName.identifire) = %@", task.identifire as CVarArg)
        
        return request
    }
    
    private func searchCompletionTask(in task: TaskCompletionRecord) -> NSFetchRequest<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataEntityName.taskCompletionRecord)
        request.predicate = NSPredicate(format: "\(CDTaskCompletionProperties.taskId) = %@", task.taskId as CVarArg)
        
        return request
    }
    
    internal func clearStorage(_ entity: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let results = try persistentContainer.viewContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else { continue }
                persistentContainer.viewContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    
    private func fetchManagedObject<T: NSManagedObject>(managedObject: T.Type) -> [T]? {
        let context = persistentContainer.viewContext
        do {
            guard let result = try context.fetch(managedObject.fetchRequest()) as? [T] else { return nil }
            
            return result
            
        } catch let error {
            debugPrint(error)
        }
        
        return nil
    }
}
