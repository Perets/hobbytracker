//
//  CoreDataConstants.swift
//  HobbyTracker
//
//  Created by Игорь Перец on 25.02.2021.
//

import Foundation

enum CoreDataEntityName {
    static let coreDataTask = "CoreDataTask"
    static let taskCompletionRecord = "CDTaskCompletionRecord"
}

enum CoreDataPropertiesName {
    static let taskText = "taskText"
    static let startingTaskDate = "startingTaskDate"
    static let finalTaskDate = "finalTaskDate"
    static let repetitionDate = "repetitionDate"
    static let state = "state"
    static let identifire = "identifire"
}

enum CDTaskCompletionProperties {
    static let taskId = "taskId"
    static let dateOfCompletion = "dateOfCompletion"
}
